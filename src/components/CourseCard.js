import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// check to see if data was successfully passed
	// console.log(props);
		// result: php-laravel (coursesData[0])
	//every component receives information in a form of object
	// console.log(typeof props);

	// object destructuring
	const {name, description, price, _id} = courseProp;

	// react hooks- useState -> store its state
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(10);
	// console.log(useState(0));

	// function enroll(){
	// 	if(seats > 0){
	// 		setCount(count + 1);
	// 		console.log(`Enrollees: ${count}`);
	// 		setSeats(seats - 1);
	// 		console.log(`Seats: ${seats}`);
	// 	} else {
	// 		alert('No more seats available, check back later.')
	// 	}
	// };

	return(
		<Card className="p-3 mb-3">
			<Card.Body>
				<Card.Title className="fw-bold">{name}</Card.Title>
				<Card.Subtitle> Course Description: </Card.Subtitle>
				<Card.Text>
					{description}
				</Card.Text>
				<Card.Subtitle> Course Price: </Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>

	)
}
